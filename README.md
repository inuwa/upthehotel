# Instructions

How to run:

```git clone https://bitbucket.org/inuwa/upthehotel```

```cd upthehotel```

```npm install; gulp```

example request : <http://localhost:3000/?url=https://www.google.com&strategy=mobile>

OR

<http://localhost:3000/?url=https://www.google.com>

###Side Note:

I could have used URL shortener to verify the url, and written tests for failed scenarios. All the above instructions can be in a deploy_script.sh for server deployment.