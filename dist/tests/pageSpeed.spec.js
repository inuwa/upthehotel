"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pageSpeed_1 = require("../controllers/pageSpeed");
const chai = require("chai");
const chaiHttp = require("chai-http");
const server_1 = require("../server");
chai.use(chaiHttp);
const expect = chai.expect;
const should = chai.should();
let pageSpeed = new pageSpeed_1.PageSpeed();
describe('Test Questions', () => {
    describe('URL Query supplied', () => {
        return chai.request(server_1.default).get('/?url=https://www.google.com').then((res) => {
            it('Should not fail with Errors', () => {
                expect(res.type).to.equal('application/json');
                res.body.response_code.should.equal(200);
                res.body.title.should.equal('Google');
            });
        });
    });
});
