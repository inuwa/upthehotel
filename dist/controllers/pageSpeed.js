"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const validUrl = require("valid-url");
const request = require("request-promise");
class PageSpeed {
    constructor() { }
    /**
     * Get the pagespeeds from the url that is sent
     * @param req request object
     * @param res response object
     */
    processLink(req, res) {
        let url = req.query.url;
        let strategy = req.query.strategy || 'desktop';
        let options = 'url=' + url + '&strategy=' + strategy;
        let pagespeedUrl = process.env.PAGESPEED_URL || 'https://www.googleapis.com/pagespeedonline/v4/runPagespeed?' + options;
        let reqOptions = {
            uri: pagespeedUrl,
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true
        };
        return new Promise((resolve, reject) => {
            if (req && res) {
                if (url) {
                    if (validUrl.isUri) {
                        request(reqOptions).then((response) => {
                            if (response && !response.error) {
                                res.json({
                                    site_url: response.id,
                                    title: response.title,
                                    speed_score: response.ruleGroups.SPEED.score,
                                    response_code: response.responseCode
                                });
                            }
                            else {
                                res.json({ error: 'Bad url: ' + url });
                                resolve();
                            }
                        }).catch(() => {
                            res.json({ error: 'Bad url: ' + url });
                            resolve();
                        });
                    }
                    else {
                        res.json({ error: 'Invalid url: ' + url });
                        resolve();
                    }
                }
                else {
                    res.json({ error: 'No url supplied. e.g url=someurl ' });
                    resolve();
                }
            }
            else {
                reject({ error: 'Server Error' });
            }
        });
    }
}
exports.PageSpeed = PageSpeed;
