import express = require('express');
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as path from 'path';
import { PageSpeed } from './controllers/pageSpeed';
import * as dotenv from 'dotenv';
 

let app = express();
let pageSpeed = new PageSpeed();
let env = process.env.NODE_ENV || 'development';
dotenv.load({ path: '.env.'+ env });
app.use(express.static(path.join(__dirname, '/../src/stylesheets/')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.get('/', (req, res) => pageSpeed.processLink(req, res));

export default app;
