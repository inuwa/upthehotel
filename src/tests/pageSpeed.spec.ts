import { PageSpeed } from '../controllers/pageSpeed';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

import app from '../server';
import { doesNotReject } from 'assert';

chai.use(chaiHttp);
const expect = chai.expect;
const should = chai.should();
let pageSpeed = new PageSpeed();
describe('Test Questions', () => {
    describe('URL Query supplied', () => {
        it('Should not fail with Errors', () => {            
            return chai.request(app).get('/?url=https://www.google.com').then((res) => {
                expect(res.type).to.equal('application/json');
                res.body.response_code.should.equal(200);
                res.body.title.should.equal('Google');
            });
        });
    });
});